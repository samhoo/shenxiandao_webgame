package org.yunai.protobuf.io;

import org.yunai.protobuf.conf.*;
import org.yunai.protobuf.util.YFUtils;
import org.yunai.protobuf.v1.Utils;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author yunai
 * @version 2012-8-6
 */
public class CodeTemplate {
	/**
	 * 生成代码 - 换行部分
	 */
	private static final String LINE_SEPARATOR = "\n";
    /**
     * 生成代码 - 间隔符
     */
    public static final String LINE_SPACE = "    ";
    public static final String LINE_SPACE_2 = LINE_SPACE + LINE_SPACE;
    public static final String LINE_SPACE_3 = LINE_SPACE + LINE_SPACE + LINE_SPACE;

	/**
	 * 生成代码 - 引入包部分变量
	 */
	private static final String REGION_IMPORT = "#{REGION_IMPORT}";

    // ==================== Decoder模版 ====================
    /**
     * 消息对象基本Decoder类的decoder模板(基础类型 + 普通级别[required])
     */
    public static final String CLASS_MESSAGE_DECODER_TYPE_1_LEVEL_1 = LINE_SPACE_3 + "#{clazz}.set#{Property}(byteArray.get#{Type}());" + LINE_SEPARATOR;
    /**
     * 消息对象基本Decoder类的decoder模板(基础类型 + 数组级别[repeated])
     */
    public static final String CLASS_MESSAGE_DECODER_TYPE_1_LEVEL_2 = LINE_SPACE_3 + "#{clazz}.set#{Property}(get#{Type}List(byteArray));" + LINE_SEPARATOR;
    /**
     * 消息对象基本Decoder类的decoder模板(字符串类型 + 普通级别[required])
     */
    public static final String CLASS_MESSAGE_DECODER_TYPE_2_LEVEL_1 = LINE_SPACE_3 + "#{clazz}.set#{Property}(getString(byteArray));" + LINE_SEPARATOR;
    /**
     * 消息对象基本Decoder类的decoder模板(字符串类型+数组级别[required])
     */
    public static final String CLASS_MESSAGE_DECODER_TYPE_2_LEVEL_2 = LINE_SPACE_3 + "#{clazz}.set#{Property}(getStringList(byteArray));" + LINE_SEPARATOR;
    /**
     * 消息对象基本Decoder类的decoder模板(Message类型 + 普通级别[required])
     */
    public static final String CLASS_MESSAGE_DECODER_TYPE_3_LEVEL_1 = LINE_SPACE_3 + "#{clazz}.set#{Property}((getMessage(#{type}.Decoder.getInstance(), byteArray, #{type}.class)));" + LINE_SEPARATOR;
    /**
     * 消息对象基本Decoder类的decoder模板(Message类型 + 数组级别[required])
     */
    public static final String CLASS_MESSAGE_DECODER_TYPE_3_LEVEL_2 = "		#{clazz}.set#{Property}(getMessageList(#{type}.Decoder.getInstance(), byteArray, #{type}.class));" + LINE_SEPARATOR;

    public static final String CLASS_MESSAGE_DECODER_REGION_DECODER_OTHER =
                      LINE_SPACE_3 + "#{class} #{clazz} = new #{class}();" + LINE_SEPARATOR
                    + "#{REGION_DECODE_SMALL}"
                    + LINE_SPACE_3 + "return #{clazz};" + LINE_SEPARATOR;

    public static final String CLASS_MESSAGE_DECODER_REGION_DECODER_NULL =
                     LINE_SPACE_3 + "return new #{class}();" + LINE_SEPARATOR;

    /**
     * 消息对象基本Decoder类的模板
     */
    public static final String CLASS_MESSAGE_DECODER_BASE =
                    LINE_SPACE + "public static class Decoder extends AbstractDecoder {" + LINE_SEPARATOR
                    + LINE_SPACE_2 + "private static Decoder decoder = new Decoder();" + LINE_SEPARATOR
                    + LINE_SEPARATOR
                    + LINE_SPACE_2 + "public static Decoder getInstance() {" + LINE_SEPARATOR
                    + LINE_SPACE_3 + "return decoder;" + LINE_SEPARATOR
                    + LINE_SPACE_2 + "}" + LINE_SEPARATOR
                    + LINE_SEPARATOR
                    + LINE_SPACE_2 + "public IStruct decode(ByteArray byteArray) {" + LINE_SEPARATOR

//                    + LINE_SPACE_3 + "#{class} #{clazz} = new #{class}();" + LINE_SEPARATOR
                    + "#{REGION_DECODE}"
//                    + LINE_SPACE_3 + "return #{clazz};" + LINE_SEPARATOR

                    + LINE_SPACE_2 + "}" + LINE_SEPARATOR
                    + LINE_SPACE + "}" + LINE_SEPARATOR;

    // ==================== Encoder模版 ====================
    /**
     * 消息对象基本Encoder类的encode模板的求长度(基础类型 + 数组级别[repeated])
     */
    public static final String CLASS_MESSAGE_ENCODER_TYPE_1_LEVEL_2_LENGTH = LINE_SPACE_3 + "convert#{Type}(byteArray, #{clazz}.get#{Property}());" + LINE_SEPARATOR;
    /**
     * 消息对象基本Encoder类的encode模板的求长度(字符串类型 + 普通级别[required])
     */
    public static final String CLASS_MESSAGE_ENCODER_TYPE_2_LEVEL_1_LENGTH = LINE_SPACE_3 + "byte[] #{property}Bytes = convertString(byteArray, #{clazz}.get#{Property}());" + LINE_SEPARATOR;
    /**
     * 消息对象基本Encoder类的encode模板的求长度(字符串类型 + 数组级别[repeated])
     */
    public static final String CLASS_MESSAGE_ENCODER_TYPE_2_LEVEL_2_LENGTH = LINE_SPACE_3 + "byte[][] #{property}Bytes = convertStringList(byteArray, #{clazz}.get#{Property}());" + LINE_SEPARATOR;
    /**
     * 消息对象基本Encoder类的encode模板的求长度(Message类型 + 普通级别[required])
     */
    public static final String CLASS_MESSAGE_ENCODER_TYPE_3_LEVEL_1_LENGTH = LINE_SPACE_3 + "byte[] #{property}Bytes = convertMessage(byteArray, #{type}.Encoder.getInstance(), #{clazz}.get#{Property}());" + LINE_SEPARATOR;
    /**
     * 消息对象基本Encoder类的encode模板的求长度(Message类型 + 普通级别[required])
     */
    public static final String CLASS_MESSAGE_ENCODER_TYPE_3_LEVEL_2_LENGTH = LINE_SPACE_3 + "byte[][] #{property}Bytes = convertMessageList(byteArray, #{type}.Encoder.getInstance(), #{clazz}.get#{Property}());" + LINE_SEPARATOR;
    /**
     * 消息对象基本Encoder类的encode模板(基础类型 + 普通级别[required])
     */
    public static final String CLASS_MESSAGE_ENCODER_TYPE_1_LEVEL_1 = LINE_SPACE_3 + "byteArray.put#{Type}(#{clazz}.get#{Property}());" + LINE_SEPARATOR;
    /**
     * 消息对象基本Encoder类的encode模板(基础类型 + 数组级别[repeated])
     */
    public static final String CLASS_MESSAGE_ENCODER_TYPE_1_LEVEL_2 = LINE_SPACE_3 + "put#{Type}(byteArray, #{clazz}.get#{Property}());" + LINE_SEPARATOR;
    /**
     * 消息对象基本Encoder类的encode模板(字符串类型 + 普通级别[required])
     */
    public static final String CLASS_MESSAGE_ENCODER_TYPE_2_LEVEL_1 = LINE_SPACE_3 + "putString(byteArray, #{property}Bytes);" + LINE_SEPARATOR;
    /**
     * 消息对象基本Encoder类的encode模板(字符串类型 + 普通级别[required])
     */
    public static final String CLASS_MESSAGE_ENCODER_TYPE_2_LEVEL_2 = LINE_SPACE_3 + "putStringList(byteArray, #{property}Bytes);" + LINE_SEPARATOR;
    /**
     * 消息对象基本Encoder类的encode模板(Message类型 + 普通级别[required])
     */
    public static final String CLASS_MESSAGE_ENCODER_TYPE_3_LEVEL_1 = LINE_SPACE_3 + "putMessage(byteArray, #{property}Bytes);" + LINE_SEPARATOR;
    /**
     * 消息对象基本Encoder类的encode模板(Message类型 + 普通级别[required])
     */
    public static final String CLASS_MESSAGE_ENCODER_TYPE_3_LEVEL_2 = LINE_SPACE_3 + "putMessageList(byteArray, #{property}Bytes);" + LINE_SEPARATOR;
    /**
     * 消息对象基本Encoder类的模板
     */
    public static final String CLASS_MESSAGE_ENCODER_BASE =
                    LINE_SPACE + "public static class Encoder extends AbstractEncoder {" + LINE_SEPARATOR
                  + LINE_SPACE_2 + "private static Encoder encoder = new Encoder();" + LINE_SEPARATOR
                    + LINE_SEPARATOR
                    + LINE_SPACE_2 + "public static Encoder getInstance() {" + LINE_SEPARATOR
                    + LINE_SPACE_3 + "return encoder;" + LINE_SEPARATOR
                    + LINE_SPACE_2 + "}" + LINE_SEPARATOR
                    + LINE_SEPARATOR
                    + LINE_SPACE_2 + "public ByteArray encode(IStruct message) {" + LINE_SEPARATOR
                    + LINE_SPACE_3 + "#{class} #{clazz} = (#{class}) message;" + LINE_SEPARATOR
                    + LINE_SPACE_3 + "ByteArray byteArray = ByteArray.createNull(#{msgLength});" + LINE_SEPARATOR
                    + "#{REGION_ENCODE_LIMIT}"
                    + LINE_SPACE_3 + "byteArray.create();" + LINE_SEPARATOR
                    + "#{REGION_ENCODE}"
                    + LINE_SPACE_3 + "return byteArray;" + LINE_SEPARATOR
                    + LINE_SPACE_2 + "}" + LINE_SEPARATOR
                    + LINE_SPACE + "}";

    // ==================== 消息模版 ====================
    /**
     * 消息对象的属性
     */
    private static final String CLASS_MESSAGE_PROPERTY =
                    LINE_SPACE + "/**" + LINE_SEPARATOR
                  + LINE_SPACE + " * #{description}" + LINE_SEPARATOR
                  + LINE_SPACE + " */" + LINE_SEPARATOR
                  + LINE_SPACE + "private #{type} #{property};" + LINE_SEPARATOR;
    /**
     * 消息对象类注释
     */
    private static final String CLASS_MESSAGE_CLASS_COMMENT =
                    "/**" + LINE_SEPARATOR
                  + " * 【#{msgId}】: #{description}" + LINE_SEPARATOR
                  + " */";
    /**
     * 结构体对象类注释
     */
    private static final String CLASS_STRUCT_CLASS_COMMENT =
                    "/**" + LINE_SEPARATOR
                  + " * #{description}" + LINE_SEPARATOR
                  + " */";
    /**
     * 消息对象的属性的SETTING/GETTING方法
     */
    public static final String CLASS_MESSAGE_SETTING_GETTING =
              "	public #{type} get#{Property}() {" + LINE_SEPARATOR
            + "		return #{property};" + LINE_SEPARATOR
            + "	}" + LINE_SEPARATOR
            + LINE_SEPARATOR
            + "	public void set#{Property}(#{type} #{property}) {" + LINE_SEPARATOR
            + "		this.#{property} = #{property};" + LINE_SEPARATOR
            + "	}" + LINE_SEPARATOR;
    /**
     * 结构体对象构造方法
     */
    public static final String CLASS_STRUCT_CONSTRUCT_NULL =
                LINE_SPACE + "public #{class}() {" + LINE_SEPARATOR
              + LINE_SPACE + "}" + LINE_SEPARATOR;

    public static final String CLASS_STRUCT_CONSTRUCT_OTHER =
                      LINE_SEPARATOR
                    + LINE_SPACE + "public #{class}(#{params}) {" + LINE_SEPARATOR
                    + "#{params_set}"
                    + LINE_SPACE + "}" + LINE_SEPARATOR;
    /**
     * 结构体对象构造方法里设置属性
     */
    public static final String CLASS_STRUCT_CONSTRUCT_SET = LINE_SPACE_2 + "this.#{name} = #{name};" + LINE_SEPARATOR;

    public static final String CLASS_REGION_GET_CODE =
//                        LINE_SEPARATOR
                       LINE_SPACE + "@Override" + LINE_SEPARATOR
                     + LINE_SPACE + "public short getCode() {" + LINE_SEPARATOR
                     + LINE_SPACE_2 +  "return CODE;" + LINE_SEPARATOR
                     + LINE_SPACE + "}" + LINE_SEPARATOR
                     + LINE_SEPARATOR;

    public static final String CLASS_REGION_EXECUTE =
                       LINE_SEPARATOR + "@SuppressWarnings(\"unchecked\")" + LINE_SEPARATOR
                     +  LINE_SEPARATOR + "@Override" + LINE_SEPARATOR
                     + LINE_SPACE + "public void execute() {" + LINE_SEPARATOR
                     + LINE_SPACE_2 + "for (Command command : MessageDispatcher.getInstance().getCommands(CODE)) {" + LINE_SEPARATOR
                     + LINE_SPACE_3 + "((AbstractMinaMessageCommand) command).execute(getSession(), this);" + LINE_SEPARATOR
                     + LINE_SPACE_2 + "}" + LINE_SEPARATOR
                     + LINE_SPACE + "}" + LINE_SEPARATOR
                     + LINE_SEPARATOR;

	/**
	 * 消息对象基本类的模板
	 */
	public static final String CLASS_MESSAGE_BASE = "package #{package};" + LINE_SEPARATOR
			+ "#{REGION_IMPORT}"
			+ LINE_SEPARATOR
            + "#{REGION_CLASS_COMMENT}" + LINE_SEPARATOR
			+ "public class #{class} #{interface} {" + LINE_SEPARATOR
            + "#{REGION_STATIC_PROPERTY}" // 特殊，目前只有Message实现类使用
			+ "#{REGION_PROPERTY}"
//            + LINE_SEPARATOR
            + "#{REGION_CONSTRUCT}" + LINE_SEPARATOR
            + "#{REGION_GET_CODE}" // 特殊，目前只有Message实现类使用
            + "#{REGION_EXECUTE}" // 特殊，目前只有IMessage实现类使用
//            + LINE_SEPARATOR
			+ "#{REGION_PROPERTY_SETTING_GETTING}"
//            + LINE_SEPARATOR
            + "#{REGION_DECODER}"
            + LINE_SEPARATOR
            + "#{REGION_ENCODER}" + LINE_SEPARATOR
			+ "}";

    // ==================== 方法 ====================
    /**
     * 生成消息对象的代码
     *
     * @param side 端配置
     * @param cc 消息类配置
     * @param pack 包名
     * @return 代码字符串
     */
    public static String formatClass(ProjectConfig.Side side, CodeClass cc, String pack) {
//        String regionImport = "import " + side.getPack() + ".core.*;";
        String regionImport = "import org.yunai.yfserver.message.*;";
        String regionProperty = "";
        String regionPropertySettingGetting = "";
        Set<String> importSet = new HashSet<String>();
        boolean importList = false;
        for (Param param : cc.obtainParams()) {
            int levelType = YFUtils.defParamType(param.getType());
            String typeStr = YFUtils.convertDataObject(param.getType());
            if (!YFUtils.notImportSetExists(typeStr)) {
                importSet.add(typeStr);
            }
            if (levelType == YFUtils.PARAM_TYPE_LIST) {
                importList = true;
                typeStr = "List<" + typeStr + ">";
            }
            regionProperty += CLASS_MESSAGE_PROPERTY
                    .replace("#{type}", typeStr)
                    .replace("#{property}", param.getName())
                    .replace("#{description}", param.getDesc());
            regionPropertySettingGetting += CLASS_MESSAGE_SETTING_GETTING
                    .replace("#{type}", typeStr)
                    .replace("#{property}", param.getName())
                    .replace("#{Property}", Utils.convertFirst2Big(param.getName()));
        }
        if (cc.obtainParams().size() > 0) {
            regionProperty += LINE_SEPARATOR;
            regionPropertySettingGetting += LINE_SEPARATOR;
        }

        // 代码引入IMPORT区域
        if (importList) {
            regionImport += LINE_SEPARATOR + "import java.util.List;";
        }
        for (String importClass : importSet) {
            regionImport += LINE_SEPARATOR + "import " + side.getPack() + ".struct." + importClass + ";";
        }

        // 特殊逻辑begin
        if (cc instanceof MessageConfig.Message) {
            regionImport += LINE_SEPARATOR + "import org.yunai.yfserver.plugin.mina.command.AbstractMinaMessageCommand;";
            regionImport += LINE_SEPARATOR + "import org.yunai.swjg.server.core.message.GameMessage;";
//            regionImport += LINE_SEPARATOR + "import org.yunai.chat.server.core.GameSession;";
            regionImport += LINE_SEPARATOR + "import org.yunai.yfserver.command.MessageDispatcher;";
            regionImport += LINE_SEPARATOR + "import org.yunai.yfserver.command.Command;";
        }
        // 特殊逻辑end

        regionImport = LINE_SEPARATOR + regionImport + LINE_SEPARATOR;

        // 类注释区域
        String regionClassComment = "";
        if (cc instanceof MessageConfig.Message) {
            MessageConfig.Message messageCfg = (MessageConfig.Message) cc;
            regionClassComment = CLASS_MESSAGE_CLASS_COMMENT
                    .replace("#{msgId}", String.valueOf(messageCfg.getId()))
                    .replace("#{description}", messageCfg.getDesc());
        } else if (cc instanceof StructConfig.Struct) {
            StructConfig.Struct structCfg = (StructConfig.Struct) cc;
            regionClassComment = CLASS_STRUCT_CLASS_COMMENT
                    .replace("#{description}", structCfg.getDesc());
        }

        // 构造方法
        String regionConstruct = CLASS_STRUCT_CONSTRUCT_NULL.replace("#{class}", cc.obtainClassName());
        if (cc.obtainParams().size() > 0) {
            String constructParams = "";
            String constructParamsSet = "";
            for (Param param : cc.obtainParams()) {
                int levelType = YFUtils.defParamType(param.getType());
                String typeStr = YFUtils.convertDataObject(param.getType());
                if (levelType == YFUtils.PARAM_TYPE_LIST) {
                    typeStr = "List<" + typeStr + ">";
                }
                if (constructParams.length() > 0) {
                    constructParams += ", ";
                }
                constructParams += (typeStr + " " + param.getName());
                constructParamsSet += CLASS_STRUCT_CONSTRUCT_SET.replace("#{name}", param.getName());
            }
            regionConstruct += CLASS_STRUCT_CONSTRUCT_OTHER.replace("#{params}", constructParams)
                    .replace("#{params_set}", constructParamsSet)
                    .replace("#{class}", cc.obtainClassName());

        }

        // 接口
        String interfaceName = "";
        if (cc instanceof MessageConfig.Message) {
//            interfaceName = org.yunai.yfserver.message.IMessage.class.getSimpleName();
            interfaceName = "extends GameMessage";
        } else if (cc instanceof StructConfig.Struct) {
            interfaceName = "implements " + org.yunai.yfserver.message.IStruct.class.getSimpleName();
        }

//        // REGION_STATIC_PROPERTY特殊逻辑
        String regionStaticProperty = "";
        if (cc instanceof MessageConfig.Message) {
            MessageConfig.Message messageCfg = (MessageConfig.Message) cc;
            regionStaticProperty = LINE_SPACE + "public static final short CODE = " + messageCfg.getId() + ";" + LINE_SEPARATOR + LINE_SEPARATOR;
        }
        // REGION_GET_CODE特殊逻辑
        String regionGetCode = "";
        if (cc instanceof MessageConfig.Message) {
//            MessageConfig.Message messageCfg = (MessageConfig.Message) cc;
            regionGetCode = CLASS_REGION_GET_CODE;
        }
        String regionExecute = "";
        if (cc instanceof MessageConfig.Message) {
            regionExecute = CLASS_REGION_EXECUTE;
        }

        // DECODER区域
        String regionDecoder = formatDecoderClass(side, cc);
        // ENCODER区域
        String regionEncoder = formatEncoderClass(side, cc);

        return CLASS_MESSAGE_BASE.replace("#{package}", pack)
                .replace(REGION_IMPORT, regionImport)
                .replace("#{class}", cc.obtainClassName())
                .replace("#{interface}", interfaceName)
                .replace("#{REGION_CLASS_COMMENT}", regionClassComment)
                .replace("#{REGION_STATIC_PROPERTY}", regionStaticProperty)
                .replace("#{REGION_GET_CODE}", regionGetCode)
                .replace("#{REGION_EXECUTE}", regionExecute)
                .replace("#{REGION_PROPERTY}", regionProperty)
                .replace("#{REGION_CONSTRUCT}", regionConstruct)
                .replace("#{REGION_DECODER}", regionDecoder)
                .replace("#{REGION_ENCODER}", regionEncoder)
                .replace("#{REGION_PROPERTY_SETTING_GETTING}", regionPropertySettingGetting);
    }

    public static String formatDecoderClass(ProjectConfig.Side side, CodeClass cc) {
        String regionDecode = "";
        if (cc.obtainParams().size() > 0) {
            for (Param param : cc.obtainParams()) {
                int dataType = YFUtils.defDataType(param.getType());
                int levelType = YFUtils.defParamType(param.getType());
                if (dataType == YFUtils.DATA_TYPE_SIMPLE) {
                    if (levelType == YFUtils.PARAM_TYPE_ONE) {
                        regionDecode += CLASS_MESSAGE_DECODER_TYPE_1_LEVEL_1
                                .replace("#{clazz}", "struct")
                                .replace("#{Property}", YFUtils.convertFirst2Big(param.getName()))
                                .replace("#{Type}", param.getType());
                    } else if (levelType == Utils.PARAM_TYPE_LIST) {
                        regionDecode += CLASS_MESSAGE_DECODER_TYPE_1_LEVEL_2
                                .replace("#{clazz}", "struct")
                                .replace("#{Property}", Utils.convertFirst2Big(param.getName()))
                                .replace("#{Type}", param.getType());
                    }
                } else if (dataType == Utils.DATA_TYPE_STRING) {
                    if (levelType == Utils.PARAM_TYPE_ONE) {
                        regionDecode += CLASS_MESSAGE_DECODER_TYPE_2_LEVEL_1
                                .replace("#{clazz}", "struct")
                                .replace("#{Property}", Utils.convertFirst2Big(param.getName()));
                    } else if (levelType == Utils.PARAM_TYPE_LIST) {
                        regionDecode += CLASS_MESSAGE_DECODER_TYPE_2_LEVEL_2
                                .replace("#{clazz}", "struct")
                                .replace("#{Property}", Utils.convertFirst2Big(param.getName()));
                    }
                } else if (dataType == Utils.DATA_TYPE_STRUCT) {
                    if (levelType == Utils.PARAM_TYPE_ONE) {
                        regionDecode += CLASS_MESSAGE_DECODER_TYPE_3_LEVEL_1
                                .replace("#{clazz}", "struct")
                                .replace("#{Property}", Utils.convertFirst2Big(param.getName()))
                                .replace("#{type}", param.getType())
                                .replace("#{class}", param.getName());
                    } else if (levelType == Utils.PARAM_TYPE_LIST) {
                        regionDecode += CLASS_MESSAGE_DECODER_TYPE_3_LEVEL_2
                                .replace("#{clazz}", "struct")
                                .replace("#{Property}", Utils.convertFirst2Big(param.getName()))
                                .replace("#{type}", YFUtils.convertDataObject(param.getType()))
                                .replace("#{class}", param.getName());
                    }
                }
            }
            regionDecode = CLASS_MESSAGE_DECODER_REGION_DECODER_OTHER.
                    replace("#{REGION_DECODE_SMALL}", regionDecode)
                    .replace("#{clazz}", "struct")
                    .replace("#{class}", cc.obtainClassName());
        } else {
            regionDecode = CLASS_MESSAGE_DECODER_REGION_DECODER_NULL.replace("#{class}", cc.obtainClassName());
        }
        return CLASS_MESSAGE_DECODER_BASE
                .replace("#{clazz}", "struct")
                .replace("#{class}", cc.obtainClassName())
                .replace("#{REGION_DECODE}", regionDecode);
    }

	public static String formatEncoderClass(ProjectConfig.Side side, CodeClass cc) {
		// 计算长度
		String regionEncodeLimit = "";
		short msgLength = 0; // 基础类型 + 普通级别[required] 的所有数据长度
		for (Param param : cc.obtainParams()) {
            int dataType = YFUtils.defDataType(param.getType());
            int levelType = YFUtils.defParamType(param.getType());
			if (dataType == YFUtils.DATA_TYPE_SIMPLE) {
				if (levelType == YFUtils.PARAM_TYPE_ONE) {
					msgLength += YFUtils.calParamLength(param.getType());
				} else if (levelType == YFUtils.PARAM_TYPE_LIST) {
					regionEncodeLimit += CLASS_MESSAGE_ENCODER_TYPE_1_LEVEL_2_LENGTH
                            .replace("#{clazz}", "struct")
							.replace("#{Property}", YFUtils.convertFirst2Big(param.getName()))
							.replace("#{Type}", param.getType());
				}
			} else if (dataType == YFUtils.DATA_TYPE_STRING) {
				if (levelType == YFUtils.PARAM_TYPE_ONE) {
					regionEncodeLimit += CLASS_MESSAGE_ENCODER_TYPE_2_LEVEL_1_LENGTH
                            .replace("#{clazz}", "struct")
							.replace("#{Property}", YFUtils.convertFirst2Big(param.getName()))
							.replace("#{property}", param.getName());
				} else if (levelType == YFUtils.PARAM_TYPE_LIST) {
					regionEncodeLimit += CLASS_MESSAGE_ENCODER_TYPE_2_LEVEL_2_LENGTH
                            .replace("#{clazz}", "struct")
							.replace("#{Property}", YFUtils.convertFirst2Big(param.getName()))
							.replace("#{property}", param.getName());
				}
			} else if (dataType == YFUtils.DATA_TYPE_STRUCT) {
				if (levelType == YFUtils.PARAM_TYPE_ONE) {
					regionEncodeLimit += CLASS_MESSAGE_ENCODER_TYPE_3_LEVEL_1_LENGTH
                            .replace("#{clazz}", "struct")
							.replace("#{Property}", YFUtils.convertFirst2Big(param.getName()))
							.replace("#{property}", param.getName())
							.replace("#{type}", param.getType());
				} else if (levelType == YFUtils.PARAM_TYPE_LIST) {
					regionEncodeLimit += CLASS_MESSAGE_ENCODER_TYPE_3_LEVEL_2_LENGTH
                            .replace("#{clazz}", "struct")
							.replace("#{Property}", YFUtils.convertFirst2Big(param.getName()))
							.replace("#{property}", param.getName())
							.replace("#{type}", YFUtils.convertDataObject(param.getType()));
				}
			}
		}

		// 编码对象
		String regionEncode = "";
		for (Param param : cc.obtainParams()) {
            int dataType = YFUtils.defDataType(param.getType());
            int levelType = YFUtils.defParamType(param.getType());
			if (dataType == Utils.DATA_TYPE_SIMPLE) {
				if (levelType == Utils.PARAM_TYPE_ONE) {
					regionEncode += CLASS_MESSAGE_ENCODER_TYPE_1_LEVEL_1
                            .replace("#{clazz}", "struct")
							.replace("#{Property}", Utils.convertFirst2Big(param.getName()))
							.replace("#{Type}", param.getType());
				} else if (levelType == Utils.PARAM_TYPE_LIST) {
					regionEncode += CLASS_MESSAGE_ENCODER_TYPE_1_LEVEL_2
                            .replace("#{clazz}", Utils.convertFirst2Small(param.getName()))
							.replace("#{Property}", Utils.convertFirst2Big(param.getName()))
							.replace("#{Type}", param.getType());
				}
			} else if (dataType == Utils.DATA_TYPE_STRING) {
				if (levelType == Utils.PARAM_TYPE_ONE) {
					regionEncode += CLASS_MESSAGE_ENCODER_TYPE_2_LEVEL_1
                            .replace("#{clazz}", Utils.convertFirst2Small(param.getName()))
							.replace("#{property}", param.getName());
				} else if (levelType == Utils.PARAM_TYPE_LIST) {
					regionEncode += CLASS_MESSAGE_ENCODER_TYPE_2_LEVEL_2
                            .replace("#{clazz}", Utils.convertFirst2Small(param.getName()))
							.replace("#{property}", param.getName());
				}
			} else if (dataType == Utils.DATA_TYPE_STRUCT) {
				if (levelType == Utils.PARAM_TYPE_ONE) {
					regionEncode += CLASS_MESSAGE_ENCODER_TYPE_3_LEVEL_1
                            .replace("#{clazz}", Utils.convertFirst2Small(param.getName()))
							.replace("#{property}", param.getName());
				} else if (levelType == Utils.PARAM_TYPE_LIST) {
					regionEncode += CLASS_MESSAGE_ENCODER_TYPE_3_LEVEL_2
                            .replace("#{clazz}", Utils.convertFirst2Small(param.getName()))
							.replace("#{property}", param.getName());
				}
			}
		}
		return CLASS_MESSAGE_ENCODER_BASE
				.replace("#{REGION_ENCODE_LIMIT}", regionEncodeLimit)
				.replace("#{msgLength}", String.valueOf(msgLength))
				.replace("#{REGION_IMPORT}", "")
				.replace("#{REGION_ENCODE}", regionEncode)
				.replace("#{class}", cc.obtainClassName())
				.replace("#{clazz}", "struct");
	}
	
	public static void main(String[] args) throws Exception {
		throw new Exception();
	}
}
