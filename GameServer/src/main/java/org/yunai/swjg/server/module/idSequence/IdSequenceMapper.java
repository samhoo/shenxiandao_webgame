package org.yunai.swjg.server.module.idSequence;

import org.yunai.swjg.server.entity.IdSequenceEntity;
import org.yunai.yfserver.persistence.orm.Entity;
import org.yunai.yfserver.persistence.orm.mybatis.Mapper;
import org.yunai.yfserver.persistence.orm.mybatis.SaveMapper;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: yunai
 * Date: 13-4-11
 * Time: 下午2:22
 */
public interface IdSequenceMapper extends Mapper, SaveMapper {

//    @Override
//    public void insert(Entity entity);

    List<IdSequenceEntity> selectIdSequenceEntityList();

    @Override
    void update(Entity entity);
}
