package org.yunai.swjg.server.module.battle.vo;

import org.yunai.swjg.server.module.battle.BattleDef;

/**
 * 战斗目标信息
 * User: yunai
 * Date: 13-6-15
 * Time: 下午10:15
 */
public class BattleTargetInfo {

    /**
     * 目标单元编号
     */
    private int unitIndex;
    /**
     * 对目标操作类型
     */
    private BattleDef.ActionTargetOpType opType;
    /**
     * 血量改变
     */
    private int hpChange;
    /**
     * 当前士气
     */
    private int curMorale;
    /**
     * 目标是否死亡
     */
    private boolean dead;
}