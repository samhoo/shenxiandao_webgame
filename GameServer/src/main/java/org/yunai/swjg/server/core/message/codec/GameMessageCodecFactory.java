package org.yunai.swjg.server.core.message.codec;

import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFactory;
import org.apache.mina.filter.codec.ProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolEncoder;

/**
 * 游戏消息编码器/解码器工厂
 * User: yunai
 * Date: 13-1-30
 * Time: 下午5:43
 */
public class GameMessageCodecFactory implements ProtocolCodecFactory {

    private ProtocolEncoder encoder;
    private ProtocolDecoder decoder;

    public GameMessageCodecFactory() {
        decoder = new GameMessageDecoder();
        encoder = new GameMessageEncoder();
    }

    @Override
    public ProtocolEncoder getEncoder(IoSession session) throws Exception {
        return encoder;
    }

    @Override
    public ProtocolDecoder getDecoder(IoSession session) throws Exception {
        return decoder;
    }
}
