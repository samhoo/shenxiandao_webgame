package org.yunai.swjg.server.entity;

import org.yunai.yfserver.persistence.orm.Entity;

/**
 * Id序列实体
 * User: yunai
 * Date: 13-4-11
 * Time: 下午1:45
 */
public class IdSequenceEntity implements Entity<String> {

    /**
     * KEY<br />
     * 推荐使用表名
     */
    private String id;
    /**
     * 最大ID
     */
    private Integer seq;

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    public Integer getSeq() {
        return seq;
    }

    public void setSeq(Integer seq) {
        this.seq = seq;
    }
}
