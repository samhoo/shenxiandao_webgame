package org.yunai.swjg.server.core.config;

/**
 * Created with IntelliJ IDEA.
 * User: yunai
 * Date: 13-4-23
 * Time: 下午5:31
 */
public abstract class ServerConfig implements Config {

    /**
     * 服务器模式<br />
     * 0 - 生产模式<br />
     * 1 - 调试模式
     */
    private int debug = 0;

    @Override
    public boolean isDebug() {
        return debug == 0;
    }
}
