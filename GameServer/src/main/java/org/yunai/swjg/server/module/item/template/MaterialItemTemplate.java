package org.yunai.swjg.server.module.item.template;

import org.jumpmind.symmetric.csv.CsvReader;
import org.yunai.yfserver.util.CsvUtil;

import java.io.IOException;

/**
 * 材料道具模版
 * User: yunai
 * Date: 13-4-10
 * Time: 下午7:37
 */
public class MaterialItemTemplate extends ItemTemplate {

    /**
     * 合成时，缺少所需元宝
     */
    private int gold;
    /**
     * 材料出处
     */
    private int outPlace;

    public int getGold() {
        return gold;
    }

    public int getOutPlace() {
        return outPlace;
    }

    // ==================== 非set/get方法 ====================
    @Override
    protected void genTemplate(CsvReader reader) throws IOException {
        gold = CsvUtil.getInt(reader, "gold", 0);
        outPlace = CsvUtil.getInt(reader, "outPlace", 0);
    }

    @Override
    protected void check() {

    }
}