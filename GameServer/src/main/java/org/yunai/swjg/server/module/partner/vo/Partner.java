package org.yunai.swjg.server.module.partner.vo;

import org.yunai.swjg.server.core.role.RoleDef;
import org.yunai.swjg.server.entity.PartnerEntity;
import org.yunai.swjg.server.module.idSequence.IdSequenceHolder;
import org.yunai.swjg.server.module.item.vo.Item;
import org.yunai.swjg.server.module.partner.PartnerDef;
import org.yunai.swjg.server.module.partner.template.PartnerTemplate;
import org.yunai.swjg.server.module.player.vo.Player;
import org.yunai.swjg.server.core.role.Role;
import org.yunai.swjg.server.rpc.struct.StPartnerInfo;
import org.yunai.yfserver.persistence.PersistenceObject;
import org.yunai.yfserver.spring.BeanManager;
import org.yunai.yfserver.time.TimeService;

import java.util.Collections;
import java.util.List;

/**
 * 伙伴
 * User: yunai
 * Date: 13-5-30
 * Time: 上午12:56
 */
public class Partner extends Role implements PersistenceObject<Integer, PartnerEntity> {

    private static TimeService timeService;
    static {
        timeService = BeanManager.getBean(TimeService.class);
    }

    /**
     * 伙伴编号
     */
    private int id;
    /**
     * 是否在数据库中
     */
    private boolean inDB;
    /**
     * 模版编号
     */
    private PartnerTemplate template;
    /**
     * 招募时间
     */
    private long addTime;
    /**
     * 解雇时间
     */
    private long removeTime;
    /**
     * 状态<br />
     * 1 - 队伍中<br />
     * 2 - 解雇
     */
    private PartnerDef.Status status;

    private Partner(Player player) {
        super(RoleDef.Unit.PARTNER);
        super.player = player;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public Integer getId() {
        return id;
    }

    public PartnerTemplate getTemplate() {
        return template;
    }

    public long getAddTime() {
        return addTime;
    }

    public long getRemoveTime() {
        return removeTime;
    }

    public PartnerDef.Status getStatus() {
        return status;
    }

    @Override
    public void fromEntity(PartnerEntity entity) {
        this.id = entity.getId();
        this.template = PartnerTemplate.get(entity.getTemplateId());
        this.addTime = entity.getAddTime();
        this.removeTime = entity.getRemoveTime();
        this.status = PartnerDef.Status.valueOf(entity.getStatus());
        // 角色字符串属性
        super.setMedicineStrength(entity.getMedicineStrength());
        super.setMedicineStunt(entity.getMedicineStunt());
        super.setMedicineMagic(entity.getMedicineMagic());
        // 角色一级级属性
        super.setLevel(entity.getLevel());
        super.setExp(entity.getExp());
        super.setDevelopStrength(entity.getDevelopStrength());
        super.setDevelopStunt(entity.getDevelopStunt());
        super.setDevelopMagic(entity.getDevelopMagic());
    }

    @Override
    public PartnerEntity toEntity() {
        PartnerEntity entity = new PartnerEntity();
        entity.setId(id);
        entity.setPlayerId(player.getId());
        entity.setTemplateId(template.getId());
        entity.setLevel(super.getLevel());
        entity.setExp(super.getExp());
        entity.setAddTime(addTime);
        entity.setRemoveTime(removeTime);
        entity.setStatus(status.get());
        entity.setDevelopStrength(super.getDevelopStrength());
        entity.setDevelopStunt(super.getDevelopStunt());
        entity.setDevelopMagic(super.getDevelopMagic());
        entity.setMedicineStrength(getMedicineStrength());
        entity.setMedicineStunt(getMedicineStunt());
        entity.setMedicineMagic(getMedicineMagic());
        return entity;
    }

    @Override
    public boolean isInDB() {
        return inDB;
    }

    @Override
    public void setInDB(boolean inDB) {
        this.inDB = inDB;
    }

    @Override
    public Integer getUnitId() {
        return player.getId();
    }

    @Override
    protected List<Item> getEquipments() {
        return Collections.emptyList(); // TODO 以后实现
    }

    @Override
    protected BaseProperties getProperties() {
        return new BaseProperties(template.getBaseStrength(), template.getBaseStunt(),
                template.getBaseMagic(), template.getBaseHp());
    }

    @Override
    protected void onModified() {
        player.save(this);
    }

    // ==================== 业务方法BEGIN ====================
    public static Partner build(Player player, PartnerEntity entity) {
        Partner partner = new Partner(player);
        partner.inDB = true;
        partner.fromEntity(entity);
        partner.setVocation(partner.getTemplate().getVocation());
        return partner;
    }

    public static Partner save(Player player, PartnerTemplate template) {
        Partner partner = new Partner(player);
        partner.id = IdSequenceHolder.genPlayerOrPartnerId();
        partner.template = template;
        partner.setDevelopStrength(0);
        partner.setDevelopStunt(0);
        partner.setDevelopMagic(0);
        partner.setLevel((short) 1);
        partner.setExp(0);
        partner.addTime = timeService.now();
        partner.removeTime = 0;
        partner.status = PartnerDef.Status.RECRUIT;
        partner.setVocation(partner.getTemplate().getVocation());
        return partner;
    }

    public void reRecruit() {
        status = PartnerDef.Status.RECRUIT;
        removeTime = 0;
        player.save(this);
    }

    public void fire() {
        status = PartnerDef.Status.FIRE;
        removeTime = timeService.now();
        player.save(this);
    }

    public StPartnerInfo genStPartnerInfo() {
        return new StPartnerInfo(id, template.getId());
    }

}
