package org.yunai.swjg.server.rpc.struct;

import org.yunai.yfserver.message.*;

/**
 * BOSS活动玩家每条排行版结构体
 */
public class StActivityBossPlayerRank implements IStruct {
    /**
     * 对BOSS造成伤害
     */
    private Integer damage;
    /**
     * 玩家昵称
     */
    private String nickname;

    public StActivityBossPlayerRank() {
    }

    public StActivityBossPlayerRank(Integer damage, String nickname) {
        this.damage = damage;
        this.nickname = nickname;
    }

	public Integer getDamage() {
		return damage;
	}

	public void setDamage(Integer damage) {
		this.damage = damage;
	}
	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

    public static class Decoder extends AbstractDecoder {
        private static Decoder decoder = new Decoder();

        public static Decoder getInstance() {
            return decoder;
        }

        public IStruct decode(ByteArray byteArray) {
            StActivityBossPlayerRank struct = new StActivityBossPlayerRank();
            struct.setDamage(byteArray.getInt());
            struct.setNickname(getString(byteArray));
            return struct;
        }
    }

    public static class Encoder extends AbstractEncoder {
        private static Encoder encoder = new Encoder();

        public static Encoder getInstance() {
            return encoder;
        }

        public ByteArray encode(IStruct message) {
            StActivityBossPlayerRank struct = (StActivityBossPlayerRank) message;
            ByteArray byteArray = ByteArray.createNull(4);
            byte[] nicknameBytes = convertString(byteArray, struct.getNickname());
            byteArray.create();
            byteArray.putInt(struct.getDamage());
            putString(byteArray, nicknameBytes);
            return byteArray;
        }
    }
}