package org.yunai.swjg.server.rpc.message.S_C;

import org.yunai.yfserver.message.*;
import org.yunai.yfserver.plugin.mina.command.AbstractMinaMessageCommand;
import org.yunai.swjg.server.core.message.GameMessage;
import org.yunai.yfserver.command.MessageDispatcher;
import org.yunai.yfserver.command.Command;

/**
 * 【21207】: 玩家货币增加消息
 */
public class S_C_PlayerAddCurrencyResp extends GameMessage {
    public static final short CODE = 21207;

    /**
     * 增加货币类型
     */
    private Byte currency;
    /**
     * 增加数量
     */
    private Integer incrNum;

    public S_C_PlayerAddCurrencyResp() {
    }

    public S_C_PlayerAddCurrencyResp(Byte currency, Integer incrNum) {
        this.currency = currency;
        this.incrNum = incrNum;
    }

    @Override
    public short getCode() {
        return CODE;
    }


@SuppressWarnings("unchecked")

@Override
    public void execute() {
        for (Command command : MessageDispatcher.getInstance().getCommands(CODE)) {
            ((AbstractMinaMessageCommand) command).execute(getSession(), this);
        }
    }

	public Byte getCurrency() {
		return currency;
	}

	public void setCurrency(Byte currency) {
		this.currency = currency;
	}
	public Integer getIncrNum() {
		return incrNum;
	}

	public void setIncrNum(Integer incrNum) {
		this.incrNum = incrNum;
	}

    public static class Decoder extends AbstractDecoder {
        private static Decoder decoder = new Decoder();

        public static Decoder getInstance() {
            return decoder;
        }

        public IStruct decode(ByteArray byteArray) {
            S_C_PlayerAddCurrencyResp struct = new S_C_PlayerAddCurrencyResp();
            struct.setCurrency(byteArray.getByte());
            struct.setIncrNum(byteArray.getInt());
            return struct;
        }
    }

    public static class Encoder extends AbstractEncoder {
        private static Encoder encoder = new Encoder();

        public static Encoder getInstance() {
            return encoder;
        }

        public ByteArray encode(IStruct message) {
            S_C_PlayerAddCurrencyResp struct = (S_C_PlayerAddCurrencyResp) message;
            ByteArray byteArray = ByteArray.createNull(5);
            byteArray.create();
            byteArray.putByte(struct.getCurrency());
            byteArray.putInt(struct.getIncrNum());
            return byteArray;
        }
    }
}