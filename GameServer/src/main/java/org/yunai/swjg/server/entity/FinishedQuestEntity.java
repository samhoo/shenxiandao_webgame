package org.yunai.swjg.server.entity;

import org.yunai.yfserver.persistence.orm.Entity;

/**
 * 完成的任务实体
 * User: yunai
 * Date: 13-5-10
 * Time: 下午4:51
 */
public class FinishedQuestEntity implements Entity<Integer> {

    /**
     * 编号
     */
    private Integer id;
    /**
     * 玩家编号
     */
    private Integer playerId;
    /**
     * 任务模版编号
     */
    private Integer questId;
    /**
     * 接受任务时间
     */
    private Long acceptTime;
    /**
     * 完成任务时间
     */
    private Long finishTime;
    /**
     * 总共完成次数
     */
    private Integer totalFinishCount;
    /**
     * 完成任务那天完成次数
     */
    private Integer todayFinishCount;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }

    public Integer getQuestId() {
        return questId;
    }

    public void setQuestId(Integer questId) {
        this.questId = questId;
    }

    public Long getAcceptTime() {
        return acceptTime;
    }

    public void setAcceptTime(Long acceptTime) {
        this.acceptTime = acceptTime;
    }

    public Long getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(Long finishTime) {
        this.finishTime = finishTime;
    }

    public Integer getTotalFinishCount() {
        return totalFinishCount;
    }

    public void setTotalFinishCount(Integer totalFinishCount) {
        this.totalFinishCount = totalFinishCount;
    }

    public Integer getTodayFinishCount() {
        return todayFinishCount;
    }

    public void setTodayFinishCount(Integer todayFinishCount) {
        this.todayFinishCount = todayFinishCount;
    }
}
