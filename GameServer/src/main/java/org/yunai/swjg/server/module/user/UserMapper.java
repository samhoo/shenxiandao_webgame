package org.yunai.swjg.server.module.user;

import org.yunai.swjg.server.entity.User;
import org.yunai.yfserver.persistence.orm.mybatis.Mapper;

/**
 * 用户数据访问类
 * User: yunai
 * Date: 13-3-28
 * Time: 下午7:49
 */
public interface UserMapper extends Mapper {

    /**
     * 根据用户名获得用户帐号信息
     * @param userName 用户名
     * @return 用户帐号信息
     */
    User selectUserByUserName(String userName);

    /**
     * 插入用户
     * @param user 用户信息
     */
    void insertUser(User user);
}
