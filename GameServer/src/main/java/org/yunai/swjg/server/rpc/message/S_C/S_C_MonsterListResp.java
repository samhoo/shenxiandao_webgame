package org.yunai.swjg.server.rpc.message.S_C;

import org.yunai.yfserver.message.*;
import java.util.List;
import org.yunai.swjg.server.rpc.struct.StVisibleMonsterInfo;
import org.yunai.yfserver.plugin.mina.command.AbstractMinaMessageCommand;
import org.yunai.swjg.server.core.message.GameMessage;
import org.yunai.yfserver.command.MessageDispatcher;
import org.yunai.yfserver.command.Command;

/**
 * 【20201】: 怪物列表响应
 */
public class S_C_MonsterListResp extends GameMessage {
    public static final short CODE = 20201;

    /**
     * 怪物结构体数组
     */
    private List<StVisibleMonsterInfo> monsters;

    public S_C_MonsterListResp() {
    }

    public S_C_MonsterListResp(List<StVisibleMonsterInfo> monsters) {
        this.monsters = monsters;
    }

    @Override
    public short getCode() {
        return CODE;
    }


@SuppressWarnings("unchecked")

@Override
    public void execute() {
        for (Command command : MessageDispatcher.getInstance().getCommands(CODE)) {
            ((AbstractMinaMessageCommand) command).execute(getSession(), this);
        }
    }

	public List<StVisibleMonsterInfo> getMonsters() {
		return monsters;
	}

	public void setMonsters(List<StVisibleMonsterInfo> monsters) {
		this.monsters = monsters;
	}

    public static class Decoder extends AbstractDecoder {
        private static Decoder decoder = new Decoder();

        public static Decoder getInstance() {
            return decoder;
        }

        public IStruct decode(ByteArray byteArray) {
            S_C_MonsterListResp struct = new S_C_MonsterListResp();
		struct.setMonsters(getMessageList(StVisibleMonsterInfo.Decoder.getInstance(), byteArray, StVisibleMonsterInfo.class));
            return struct;
        }
    }

    public static class Encoder extends AbstractEncoder {
        private static Encoder encoder = new Encoder();

        public static Encoder getInstance() {
            return encoder;
        }

        public ByteArray encode(IStruct message) {
            S_C_MonsterListResp struct = (S_C_MonsterListResp) message;
            ByteArray byteArray = ByteArray.createNull(0);
            byte[][] monstersBytes = convertMessageList(byteArray, StVisibleMonsterInfo.Encoder.getInstance(), struct.getMonsters());
            byteArray.create();
            putMessageList(byteArray, monstersBytes);
            return byteArray;
        }
    }
}