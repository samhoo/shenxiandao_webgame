package org.yunai.swjg.server.module.quest;

import org.springframework.stereotype.Component;
import org.yunai.swjg.server.module.quest.vo.DoingQuest;
import org.yunai.yfserver.async.IIoOperationService;
import org.yunai.yfserver.persistence.PersistenceObject;
import org.yunai.yfserver.persistence.async.DeletePersistenceObjectOperation;
import org.yunai.yfserver.persistence.async.SavePersistenceObjectOperation;
import org.yunai.yfserver.persistence.updater.PersistenceObjectUpdater;

import javax.annotation.Resource;

/**
 * 玩家进行中任务的数据持久更新器
 * User: yunai
 * Date: 13-5-10
 * Time: 下午7:34
 */
@Component
public class DoingQuestUpdater implements PersistenceObjectUpdater {

    @Resource
    private DoingQuestMapper doingQuestMapper;
    @Resource
    private IIoOperationService ioOperationService;

    @Override
    public void save(PersistenceObject<?, ?> obj) {
        ioOperationService.asyncExecute(new SavePersistenceObjectOperation<>((DoingQuest) obj, doingQuestMapper));
    }

    @Override
    public void delete(PersistenceObject<?, ?> obj) {
        ioOperationService.asyncExecute(new DeletePersistenceObjectOperation<>((DoingQuest) obj, doingQuestMapper));
    }
}
