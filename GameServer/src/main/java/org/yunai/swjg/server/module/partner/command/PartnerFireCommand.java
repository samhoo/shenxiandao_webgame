package org.yunai.swjg.server.module.partner.command;

import org.springframework.stereotype.Controller;
import org.yunai.swjg.server.core.service.GameMessageCommand;
import org.yunai.swjg.server.core.service.Online;
import org.yunai.swjg.server.module.partner.PartnerService;
import org.yunai.swjg.server.rpc.message.C_S.C_S_PartnerFireReq;

import javax.annotation.Resource;

/**
 * 伙伴解雇命令
 * User: yunai
 * Date: 13-5-31
 * Time: 下午5:18
 */
@Controller
public class PartnerFireCommand extends GameMessageCommand<C_S_PartnerFireReq> {

    @Resource
    private PartnerService partnerService;

    @Override
    protected void execute(Online online, C_S_PartnerFireReq msg) {
        partnerService.fire(online, msg.getId());
    }
}
