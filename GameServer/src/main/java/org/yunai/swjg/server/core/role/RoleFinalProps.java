package org.yunai.swjg.server.core.role;

import org.yunai.yfserver.annotation.Type;

/**
 * 角色在游戏过程中对客户端不可见的属性
 * User: yunai
 * Date: 13-5-2
 * Time: 下午8:17
 */
public class RoleFinalProps extends PropertyObject {

    public static final int BEGIN = 0;
    public static int END = BEGIN;

    /**
     * 最后一次登录时间，单位（毫秒）
     */
    @Type(Long.class)
    public static final int LAST_LOGIN_TIME = END++;
    /**
     * 最后一次登出时间，单位（毫秒）
     */
    @Type(Long.class)
    public static final int LAST_LOGOUT_TIME = END++;
    /**
     * 最后一次登录IP
     */
    @Type(String.class)
    public static final int LAST_IP = END++;
    /**
     * 角色创建时间，单位（毫秒）
     */
    @Type(Long.class)
    public static final int CREATE_ROLE_TIME = END++;
    /**
     * 总在线时长，单位（毫秒）
     */
    @Type(Long.class)
    public static final int TOTAL_ONLINE_TIME = END++;
    /**
     * 上上一次登录，单位（毫秒）
     */
    @Type(Long.class)
    public static final int LAST_LAST_LOGIN_TIME = END++;
    /**
     * 上上一次IP
     */
    @Type(String.class)
    public static final int LAST_LAST_IP = END++;

    /**
     * 属性个数
     */
    public static final int SIZE = END;
    /**
     * 属性类型
     */
    public static final RoleDef.PropertyType TYPE = RoleDef.PropertyType.ROLE_PROPS_FINAL;

    public RoleFinalProps() {
        super(SIZE, TYPE);
    }
}