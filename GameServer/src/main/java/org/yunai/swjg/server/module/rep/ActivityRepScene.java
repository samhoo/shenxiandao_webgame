package org.yunai.swjg.server.module.rep;

import org.yunai.swjg.server.core.annotation.SceneThread;
import org.yunai.swjg.server.core.constants.SceneConstants;
import org.yunai.swjg.server.core.heartbeat.HeartbeatTask;
import org.yunai.swjg.server.core.heartbeat.HeartbeatTaskExecutor;
import org.yunai.swjg.server.core.heartbeat.HeartbeatTaskExecutorImpl;
import org.yunai.swjg.server.core.service.Online;
import org.yunai.swjg.server.module.activity.ActivityService;
import org.yunai.swjg.server.module.monster.vo.VisibleMonster;
import org.yunai.swjg.server.module.rep.template.RepTemplate;
import org.yunai.swjg.server.rpc.message.S_C.S_C_MonsterHpReduceResp;
import org.yunai.yfserver.spring.BeanManager;
import org.yunai.yfserver.util.MathUtils;

/**
 * 活动副本场景
 * User: yunai
 * Date: 13-5-20
 * Time: 下午7:38
 */
public class ActivityRepScene extends RepScene {

    private static ActivityService activityService;
    static {
        activityService = BeanManager.getBean(ActivityService.class);
    }

    /**
     * 心跳执行器
     */
    private HeartbeatTaskExecutor taskExecutor;

    public ActivityRepScene(RepTemplate repTemplate) {
        super(repTemplate);
        taskExecutor = new HeartbeatTaskExecutorImpl();
    }

    @Override
    public void onPlayerEnter(Online online) {
        super.onPlayerEnter(online);
        switch (getSceneId()) {
            case SceneConstants.SCENE_BOSS_ACTIVITY_A:
                activityService.getBossActivityA().afterJoin(online);
                break;
        }
    }

    /**
     * 广播怪物掉血消息给场景中的玩家<br />
     * 该方法请在场景线程中调用
     *
     * @param monster 怪物
     * @param lossHp 掉血
     */
    @SceneThread
    public void broadcastMonsterHpReduceMessage(VisibleMonster monster, Integer lossHp) {
        S_C_MonsterHpReduceResp monsterHpReduceResp = new S_C_MonsterHpReduceResp(MathUtils.ceil2Int(monster.getHpCur()),
                MathUtils.ceil2Int(monster.getHpMax()), monster.getId(), lossHp);
        super.broadcastMessage(monsterHpReduceResp);
    }

    @Override
    public void heartBeat() {
        super.heartBeat();
        taskExecutor.heartBeat();
    }

    /**
     * 添加心跳任务
     *
     * @param task 任务
     */
    public void addTask(HeartbeatTask task) {
        taskExecutor.submit(task);
    }
}


/**
 * 开启：
 * 1. 定时器发送开启活动副本。
**/

/**
 * 关闭:
 * 关闭条件：没人 + 活动结束
 * 关闭方式：自动清理
**/

/**
 * 进入：
 * 1. 进入该活动的最后一个该副本，即{@link org.yunai.swjg.server.module.rep.RepSceneService#sceneRunners}对应的value数组的最后一个元素
**/

