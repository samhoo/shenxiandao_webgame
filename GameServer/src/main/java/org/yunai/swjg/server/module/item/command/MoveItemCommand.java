package org.yunai.swjg.server.module.item.command;

import org.springframework.stereotype.Component;
import org.yunai.swjg.server.core.service.GameMessageCommand;
import org.yunai.swjg.server.core.service.Online;
import org.yunai.swjg.server.module.item.ItemService;
import org.yunai.swjg.server.rpc.message.C_S.C_S_MoveItemReq;

import javax.annotation.Resource;

/**
 * 道具移动命令
 * User: yunai
 * Date: 13-4-12
 * Time: 下午7:18
 */
@Component
public class MoveItemCommand extends GameMessageCommand<C_S_MoveItemReq> {

    @Resource
    private ItemService itemService;

    @Override
    public void execute(Online online, C_S_MoveItemReq msg) {
        itemService.moveItem(online, msg.getFromWearId(), msg.getFromBagId(), msg.getFromBagIndex(),
                msg.getToWearId(), msg.getToBagId(), msg.getToBagIndex());
    }
}