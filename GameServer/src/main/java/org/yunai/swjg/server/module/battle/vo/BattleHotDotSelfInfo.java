package org.yunai.swjg.server.module.battle.vo;

/**
 * 战斗Hot/Dot效果【自己】信息<br />
 * User: yunai
 * Date: 13-6-15
 * Time: 下午9:57
 */
public class BattleHotDotSelfInfo {

    /**
     * Hot/Dot编号
     */
    private int sn;
    /**
     * 血量变化
     */
    private int hpChange;
    /**
     * 目标是否死亡
     */
    private boolean dead;

    public BattleHotDotSelfInfo(int sn, int hpChange) {
        this.sn = sn;
        this.hpChange = hpChange;
    }

    public int getSn() {
        return sn;
    }

    public void setSn(int sn) {
        this.sn = sn;
    }

    public int getHpChange() {
        return hpChange;
    }

    public void setHpChange(int hpChange) {
        this.hpChange = hpChange;
    }

    public boolean isDead() {
        return dead;
    }

    public void setDead(boolean dead) {
        this.dead = dead;
    }
}
