package org.yunai.swjg.server.module.quest.vo;

import org.yunai.swjg.server.entity.FinishedQuestEntity;
import org.yunai.swjg.server.module.idSequence.IdSequenceHolder;
import org.yunai.swjg.server.module.player.vo.Player;
import org.yunai.swjg.server.module.quest.AbstractQuest;
import org.yunai.yfserver.persistence.PersistenceObject;

/**
 * 玩家已经完成的任务
 * User: yunai
 * Date: 13-5-10
 * Time: 下午4:48
 */
public class FinishedQuest implements PersistenceObject<Integer, FinishedQuestEntity> {

    /**
     * 是否在数据库中
     */
    private boolean inDB;
    /**
     * 编号
     */
    private Integer id;
    /**
     * 玩家信息
     */
    private Player player;
    /**
     * 任务
     */
    private AbstractQuest quest;
    /**
     * 接受任务时间
     */
    private Long acceptTime;
    /**
     * 完成任务时间
     */
    private Long finishTime;
    /**
     * 总共完成次数
     */
    private Integer totalFinishCount;
    /**
     * 完成任务那天完成次数
     */
    private Integer todayFinishCount;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void fromEntity(FinishedQuestEntity entity) {
        id = entity.getId();
        acceptTime = entity.getAcceptTime();
        finishTime = entity.getFinishTime();
        totalFinishCount = entity.getTotalFinishCount();
        todayFinishCount = entity.getTodayFinishCount();
    }

    @Override
    public FinishedQuestEntity toEntity() {
        FinishedQuestEntity entity = new FinishedQuestEntity();
        entity.setId(id);
        entity.setPlayerId(player.getId());
        entity.setQuestId(quest.getId());
        entity.setAcceptTime(acceptTime);
        entity.setFinishTime(finishTime);
        entity.setTotalFinishCount(totalFinishCount);
        entity.setTodayFinishCount(todayFinishCount);
        return entity;
    }

    @Override
    public boolean isInDB() {
        return inDB;
    }

    @Override
    public void setInDB(boolean inDB) {
        this.inDB = inDB;
    }

    @Override
    public Integer getUnitId() {
        return player.getId();
    }

    public AbstractQuest getQuest() {
        return quest;
    }

    // ==================== 业务方法BEGIN ====================
    public static FinishedQuest build(Player player, FinishedQuestEntity entity, AbstractQuest quest) {
        FinishedQuest finishedQuest = new FinishedQuest();
        finishedQuest.inDB = true;
        finishedQuest.player = player;
        finishedQuest.quest = quest;
        finishedQuest.fromEntity(entity);
        return finishedQuest;
    }

    public static FinishedQuest save(Player player, AbstractQuest quest, Long acceptTime, Long finishTime) {
        FinishedQuest finishedQuest = new FinishedQuest();
        finishedQuest.inDB = false;
        finishedQuest.id = IdSequenceHolder.genFinishedQuestId();
        finishedQuest.player = player;
        finishedQuest.quest = quest;
        finishedQuest.acceptTime = acceptTime;
        finishedQuest.finishTime = finishTime;
        finishedQuest.todayFinishCount = 1;
        finishedQuest.totalFinishCount = 1;
        player.save(finishedQuest);
        return finishedQuest;
    }
}
