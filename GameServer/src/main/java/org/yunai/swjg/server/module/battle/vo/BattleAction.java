package org.yunai.swjg.server.module.battle.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * 战斗一回合所有玩家行动
 * User: yunai
 * Date: 13-6-17
 * Time: 下午3:05
 */
public class BattleAction {

    /**
     * 第x回合
     */
    private final int round;
    /**
     * 战斗单元行为数组
     */
    private final List<BattleUnitAction> actions;

    public BattleAction(int round) {
        this.round = round;
        this.actions = new ArrayList<>();
    }

    public void addUnitAction(BattleUnitAction action) {
        actions.add(action);
    }
}