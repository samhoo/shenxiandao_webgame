package org.yunai.swjg.server.module.arena;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.yunai.yfserver.common.LoggerFactory;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;

/**
 * 竞技场Runner
 * User: yunai
 * Date: 13-5-26
 * Time: 下午11:21
 */
public class ArenaRunner implements Callable<Void> {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoggerFactory.Logger.arena, ArenaRunner.class);

    /**
     * 绑定的竞技场
     */
    private final Arena arena;
    /**
     * 执行中的Future
     */
    private Future<Void> future;

    public ArenaRunner(Arena arena) {
        this.arena = arena;
    }

    @Override
    public Void call() throws Exception {
        try {
            arena.tick();
        } catch (Exception e) {
            LOGGER.error("[call] [error:{}]", ExceptionUtils.getStackTrace(e));
        }
        return null;
    }

    public void setFuture(Future<Void> future) {
        this.future = future;
    }

    /**
     * 任务({@link #future}是否执行完
     *
     * @return true/false
     */
    public boolean isDone() {
        return future == null || future.isDone();
    }
}
