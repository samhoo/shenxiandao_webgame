package org.yunai.swjg.server.module.quest.condition;

import org.yunai.swjg.server.module.item.ItemParam;
import org.yunai.swjg.server.module.item.container.Bag;
import org.yunai.swjg.server.module.item.vo.Item;
import org.yunai.swjg.server.module.player.vo.Player;
import org.yunai.swjg.server.module.quest.QuestDef;
import org.yunai.swjg.server.rpc.struct.StQuestCondition;

import java.util.Arrays;
import java.util.List;

/**
 * 任务条件：给予道具
 * User: yunai
 * Date: 13-5-9
 * Time: 上午1:25
 */
public class QuestItemCondition implements IQuestCondition {

    /**
     * 道具编号
     */
    private Integer itemId;
    /**
     * 给予道具数量
     */
    private Integer count;

    public QuestItemCondition(Integer itemId, Integer count) {
        this.itemId = itemId;
        this.count = count;
    }

    @Override
    public QuestDef.Condition getCondition() {
        return QuestDef.Condition.ITEM;
    }

    @Override
    public Integer getCount() {
        return count;
    }

    @Override
    public Integer getSubject() {
        return itemId;
    }

    @Override
    public boolean isMeet(Player player, Integer questId, boolean showError) {
//        Integer doCount = online.getQuestDiary().getFinishedCount(questId, getCondition(), itemId);
        ItemParam itemParam = new ItemParam(itemId, count);
        return player.getInventory().checkBagItemEnough(Bag.BagType.PRIM, Arrays.asList(itemParam), showError);
    }

    @Override
    public void onAccept(Player player) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void onFinish(Player player) {
        ItemParam itemParam = new ItemParam(itemId, count);
        List<Item> changedItems = player.getInventory().removeItems(Bag.BagType.PRIM, Arrays.asList(itemParam));
        player.getInventory().sendModifyMessage(changedItems);
    }

    /**
     * 获得主背包中该道具的数量
     *
     * @param player 玩家信息
     * @return 道具数量
     */
    @Override
    public int initParam(Player player) {
        return player.getInventory().getPrimBag().getCountByTemplateId(itemId);
    }

    @Override
    public StQuestCondition genFinishCondition(Player player, Integer questId) {
        Integer doCount = player.getQuestDiary().getFinishedCount(questId, getCondition(), itemId);
        return new StQuestCondition(getCondition().getValue(), itemId, count, doCount);
    }
}