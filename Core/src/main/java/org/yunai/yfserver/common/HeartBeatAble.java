package org.yunai.yfserver.common;

/**
 * 心跳接口
 * User: yunai
 * Date: 13-4-22
 * Time: 下午4:24
 */
public interface HeartBeatable {

    void heartBeat();
}
